const router = require("express").Router();
const Users = require("../models/Users");
const verify = require('./verifyToken');
const redisClient = require('../redis-client')
const apicache = require('apicache');
let cache = apicache.middleware;


router.get('/v1/users/', async (req, res) => {
    const users = await Users.find();
    res.json({users})
})

router.get('/v2/users/', cache('5 minutes'), async (req, res) => {
    const users = await Users.find();
    res.json({users})
})

router.get('/v3/users/', redisCache, async (req, res) => {
    console.log('Fetching data' + req.path)
    const users = await Users.find();
    redisClient.setex(req.path, 300, JSON.stringify(users))
    res.json({users})

})

router.get('/v4/users/', verify, async (req, res) => {
    const users = await Users.find();
    res.json({users})
})

router.get('/v5/users/', verify, cache('5 minutes'), async (req, res) => {
    const users = await Users.find();
    res.json({users})
})

router.get('/v6/users/', verify, redisCache, async (req, res) => {
    console.log('Fetching data' + req.path)
    const users = await Users.find();
    redisClient.setex(req.path, 300, JSON.stringify(users))
    res.json({users})
})

function redisCache(req, res, next) {
    redisClient.get(req.path, (err, data) => {
        if (err) throw err;

        if (data !== null) {
            
            res.json(JSON.parse(data))
        } else {
            next();
        }
    });
}

module.exports = router;