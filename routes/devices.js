const router = require("express").Router();
const Devices = require("../models/Devices");
const verify = require("./verifyToken");
const { deviceValidation } = require("../validation");

router.get("/v1/devices/", async (req, res) => {
  const devices = await Devices.find();
  res.json(devices.map(device => ({id : device._id, name: device.name})));
});

router.post("/v1/devices/", verify, async (req, res) => {
  const { error } = deviceValidation(req.body);
  if (error) return res.status(400).send({ message: error.details[0].message });

  const deviceExist = await Devices.findOne({
    userId: req.user,
    name: req.body.name,
  });
  

  if (deviceExist)
    return res.status(400).send({ message: "Device already exist" });

  const newDevice = new Devices({
    userId: req.user,
    name: req.body.name,
  });

  try {
    const savedDevice = newDevice.save();
    res.sendStatus(201);
  } catch (err) {
    res.sendStatus(400).send(err);
  }
});

router.put("/v1/devices/:deviceId", verify, async (req, res) => {
    const { error } = deviceValidation(req.body);
  if (error) return res.status(400).send({ message: error.details[0].message });

    const newDevice = await Devices.findOne({
        userId: req.user,
        _id: req.params.deviceId,
      });
      if (newDevice == null) return res.status(400).send({ message: "Device not found" });

      const deviceExist = await Devices.findOne({
        userId: req.user,
        name: req.body.name,
      });
      if (deviceExist) return res.status(400).send({ message: "Device already exist" });
    
      newDevice.name = req.body.name
      try {
        const savedDevice = newDevice.save();
        res.sendStatus(201);
      } catch (err) {
        res.sendStatus(400).send(err);
      }
  });

router.delete("/v1/devices/:deviceId", verify, async (req, res) => {
  const device = await Devices.findOneAndDelete({
    userId: req.user,
    _id: req.params.deviceId,
  });
  if (device == null) return res.status(400).send({ message: "Device not found" });
  return res.sendStatus(204);
});

module.exports = router;
