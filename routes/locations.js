const router = require("express").Router();
const Locations = require("../models/Locations");
const verify = require("./verifyToken");
const { locationValidation } = require("../validation");

router.get("/v1/locations", verify, async (req, res) => {
  const locations = await Locations.find();
  res.json(locations.map(location => ({
    latitude : location.location.coordinates[1],
    longitude : location.location.coordinates[0],
    date: location.date})));
})
router.post("/v1/locations", verify, async (req, res) => {
  console.log(req.body)
  const { error } = locationValidation(req.body);
  if (error) return res.status(400).send({ message: error.details[0].message });
  
  const newLocation = Locations({
    userId: req.user,
    location: {
      type: "Point",
      coordinates: [req.body.longitude, req.body.latitude],
    },
    accuracy: req.body.accuracy,
    date: req.body.date,
  });

  try {
    const savedLocation = newLocation.save();
    res.sendStatus(201);
  } catch (err) {
    res.sendStatus(400).send(err);
  }
});
module.exports = router;
