require("dotenv").config();
const router = require("express").Router();
const Users = require("../models/Users");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const { registerValidation, loginValidation } = require("../validation");

router.post("/register", async (req, res) => {
  const { error } = registerValidation(req.body);
  if (error) return res.status(400).send({ message: error.details[0].message });

  const emailExist = await Users.findOne({ email: req.body.email });
  if (emailExist)
    return res.status(400).send({ message: "Email already exist" });

  const hashedPassword = await bcrypt.hash(req.body.password, 10);

  const users = new Users({
    name: req.body.name,
    email: req.body.email,
    password: hashedPassword,
  });

  try {
    const savedUser = users.save();
    res.sendStatus(201);
  } catch (err) {
    res.sendStatus(400).send(err);
  }
});

router.post("/login", async (req, res) => {
  const { error } = loginValidation(req.body);
  if (error) return res.status(400).send({ message: error.details[0].message });

  const user = await Users.findOne({ email: req.body.email });
  if (!user)
    return res.status(400).send({ message: "Email or password is wrong" });

  const validPass = await bcrypt.compare(req.body.password, user.password);
  if (!validPass)
    return res.status(400).send({ message: "Email or password is wrong" });

  const accressToken = generateAccessToken(user);
  const refreshToken = generateRefreshToken(user);

  res.json({ accessToken: accressToken, refreshToken: refreshToken });
});

router.delete('/logout', async(req, res) => {
    
})

function generateAccessToken(user) {
//   return jwt.sign({ _id: user._id }, process.env.ACCESS_TOKEN_SECRET, {
//     expiresIn: "15s",
//   });

  return jwt.sign({ _id: user._id }, process.env.ACCESS_TOKEN_SECRET);
}

function generateRefreshToken(user) {
  return jwt.sign({ _id: user._id }, process.env.REFRESH_TOKEN_SECRET);
}

module.exports = router;
