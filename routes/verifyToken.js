const jwt = require("jsonwebtoken");

module.exports = function(req, res, next) {
  const authorization = req.header("Authorization");
  if (!authorization) return res.status(401).send({ message: "Access Denied" });

  const token = authorization.split(" ")[1]
  try {
    const verified = jwt.verify(token, process.env.ACCESS_TOKEN_SECRET)
    req.user = verified;
    next();
  }catch(err) {
    res.status(400).send({message: "Invalid Token"})
  }
} 
