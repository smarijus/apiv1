require("dotenv").config();

const express = require("express");
const mongoose = require("mongoose");
const app = express();
const jwt = require("jsonwebtoken"); 
const authRouter = require("./routes/auth");
const userRouter = require("./routes/users");
const deviceRouter = require("./routes/devices");
const locationRouter = require("./routes/locations");
const redisClient = require('./redis-client')

app.use(express.json());
app.use("/api/v1/users", authRouter);
app.use("/api", userRouter);
app.use("/api", deviceRouter);
app.use("/api", locationRouter);

mongoose.connect(process.env.DB_CONNECT, 
  { useUnifiedTopology: true,  useNewUrlParser: true }, 
  () => console.log("Connectet to db"))

  redisClient.on("error", function(error) {
    console.error(error);
  });

const posts = [
  {
    username: "User 1",
    title: "Post 1",
  },
  { 
    username: "User 2",
    title: "Post 2",
  },
];
app.get("/loaderio-4e60255ba4e4a1fc1f4ea2fe31a055bd", (req, res) => {
  res.send('loaderio-4e60255ba4e4a1fc1f4ea2fe31a055bd')
})
app.get("/posts", authenticateToken, (req, res) => {
  res.json(posts.filter((post) => post.username === req.user.name));
});
app.get("/", (req, res) => {
  res.send("API")
});

function authenticateToken(req, res, next) {
  const authHeader = req.headers["Authorization"];
  const token = authHeader && authHeader.split(" ")[1];
  if (token == null) return res.sendStatus(401);

  jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
    if (err) return res.sendStatus(403);
    req.user = user;
    next();
  });
}

app.listen(3000, () => console.log("Server up and running"));
