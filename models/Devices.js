const mongoose = require('mongoose');

const devicesSchema = new mongoose.Schema({
    userId: {
        type: String,
        required: true,
        min:2,
        max:255
    },
    name: {
        type: String,
        required: true,
        max:255,
        min:6
    }
    
});
module.exports = mongoose.model('Devices', devicesSchema);