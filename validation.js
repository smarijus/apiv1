const Joi = require("@hapi/joi");

const registerValidation = data => {
    const schema = Joi.object({
        name: Joi.string().min(6).required(),
        email: Joi.string().min(6).required().email(),
        password: Joi.string().min(6).required(),
      });
      return schema.validate(data);
}

const loginValidation = data => {
    const schema = Joi.object({
        email: Joi.string().min(6).required().email(),
        password: Joi.string().min(6).required(),
      });
      return schema.validate(data);
}

const deviceValidation = data => {
  const schema = Joi.object({
      name: Joi.string().min(2).required()
    });
    return schema.validate(data);
}

const locationValidation = data => {
  const schema = Joi.object({
      latitude: Joi.number().required(),
      longitude: Joi.number().required(),
      accuracy: Joi.number().required(),
      date: Joi.date().required()
    });
    return schema.validate(data);
}

module.exports.registerValidation = registerValidation;
module.exports.loginValidation = loginValidation;
module.exports.deviceValidation = deviceValidation;
module.exports.locationValidation = locationValidation;



